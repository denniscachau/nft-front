export const environment = {
  production: true,
  BASE_URL: "https://api.nft.test.ps-ciren.com/api",
  UPLOAD_BASE_URL: "https://api.nft.test.ps-ciren.com"
};
