import { Inject, Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { StrapiErrorResponse } from "../interfaces/strapi-response";
import { catchError, Observable, throwError } from "rxjs";
import { AlertService } from "../services/alert.service";

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private alertService: AlertService
  ) { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
  {
    if (!req.url.match('^http[s]?/\/\/.+$') && !req.url.match('^\.\/.+$')) {
      req = req.clone({
        url: environment.BASE_URL+req.url
      });
    }

    let token = !req.body?.without_jwt ? localStorage.getItem('token') : null;
    if (token !== null) {
      req = req.clone({
        setHeaders: {
          authorization: "Bearer " + token
        }
      })
    }

    return next.handle(req)
      .pipe(
        catchError((error: HttpErrorResponse) => this.handleError(error))
      )
  }

  private handleError(response: HttpErrorResponse)
  {
    const error = response.error as StrapiErrorResponse;

    this.alertService.alert({
      type: 'danger',
      message: error.error.message
    });

    return throwError(response)
  }
}
