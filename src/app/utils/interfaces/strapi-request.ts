export interface Data {
  id?: number;
  [key: string]: any;
}
export interface StrapiRequest {
  data: Data;
}
