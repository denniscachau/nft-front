export type AlertType = "success" | "danger" | "info" | "warning" | null;

export interface Alert {
  type: AlertType,
  message: string
}

export interface AlertTrans {
  type: AlertType,
  key: string
}
