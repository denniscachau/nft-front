// SUCCESS
export interface Attribute {
  // createdAt: Date;
  // updatedAt: Date;
  [key: string]: any;
}

export interface Data {
  id: number;
  [key: string]: any;
  attributes: Attribute;
}

export interface Pagination {
  page: number;
  pageSize: number;
  pageCount: number;
  total: number;
}

export interface Meta {
  pagination: Pagination;
}

export interface StrapiGetMultipleResponse {
  data: Data[];
  meta?: Meta;
}
export interface StrapiGetResponse {
  data: Data;
  meta?: Meta;
}
export interface StrapiPostResponse {
  [key: string]: any;
}
export type StrapiPostMultipleResponse = any[];

// ERROR
export interface Error {
  status: string;
  name: string;
  message: "";
  details: any;
}

export interface StrapiErrorResponse {
  data: null;
  error: Error;
}
