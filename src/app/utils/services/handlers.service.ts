import { Injectable } from '@angular/core';
import { AlertTrans } from "../interfaces/alert";
import { TranslateService } from "@ngx-translate/core";
import { AlertService } from "./alert.service";

@Injectable({
  providedIn: 'root'
})
export class HandlersService {

  constructor(
    private translateService: TranslateService,
    private alertService: AlertService
  ) { }

  public alert(alert: AlertTrans): void
  {
    this.translateService.get(["alert.close", alert.key]).subscribe(translates => {
      this.alertService.alert({
        type: alert.type,
        message: translates[alert.key]
      }, translates['alert.close']);
    });
  }

  public confirm(messageKey: string): boolean
  {
    return window.confirm(this.translateService.instant(messageKey));
  }

}
