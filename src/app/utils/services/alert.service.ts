import { Injectable } from '@angular/core';
import {Alert} from "../interfaces/alert";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private _snackBar: MatSnackBar,
  ) { }

  public alert(alert: Alert, action: string = "X"): void
  {
    this._snackBar.open(
      alert.message,
      action,
      {
        panelClass: "growl-"+alert.type,
        duration: 3000
      }
    );
  }

}
