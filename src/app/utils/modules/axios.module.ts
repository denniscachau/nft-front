import axios from "axios";

let token = localStorage.getItem("token");

export default axios.create({
  baseURL: 'http://127.0.0.1:1337/api/',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: "Bearer " + token
  }
});
