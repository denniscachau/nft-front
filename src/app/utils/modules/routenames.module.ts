export class RoutenamesModule {
  public static routeNamesObject: any = {}

  public static getRoutePath(name: string, params:any = {}): string {
    if (typeof this.routeNamesObject[name] !== 'undefined') {
      let route = this.routeNamesObject[name];
      let path = route.path;
      for (let param of Object.keys(params)) {
        let regex = new RegExp("(:" + param + ")")
        path = path.replace(regex, params[param]);
      }
      return path
    } else {
      return ""
    }
  }
}
