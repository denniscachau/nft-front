import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "../../auth/services/auth.service";
import { HandlersService } from "../services/handlers.service";

@Injectable({
  providedIn: 'root'
})
export class IsAuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private handlersService: HandlersService
  ) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authService.isLogged() !== null) {
      return true;
    }

    this.handlersService.alert({ type: "danger", key: "alert.error.forbidden-page" })

    return false;
  }

}
