import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "../../auth/services/auth.service";
import { RoutenamesModule } from "../modules/routenames.module";
import { SaveService } from "../../editor/services/save.service";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Injectable({
  providedIn: 'root'
})
export class HasCollectionsGuard implements CanActivate {

  private routeNames = RoutenamesModule

  constructor(
    private authService: AuthService,
    private router: Router,
    private saveService: SaveService,
    private ngxLoader: NgxUiLoaderService
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.authService.isLogged()) {
      return true;
    } else {
      let uuid = this.saveService.getLocalStorageSave();
      this.ngxLoader.start();
      if (!uuid) {
        this.saveService.createSave();
        return false;
      } else {
        return this.router.navigate([
          this.routeNames.getRoutePath('editor', { uuid })
        ])
      }
    }
  }

}
