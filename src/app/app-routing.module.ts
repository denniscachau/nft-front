import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';

import { HomeComponent } from "./home/components/home/home.component";
import { RoutenamesModule } from "./utils/modules/routenames.module";
import { EditorComponent } from "./editor/components/editor/editor.component";
import { CollectionComponent } from "./collection/components/collection/collection.component";
import { DownloadComponent } from "./download/components/download/download.component";
import { HasCollectionsGuard } from "./utils/guards/has-collections.guard";

const routes: Routes = [
  { path: '', children: [
      { path: 'editor/:uuid', component: EditorComponent, data: { routeName: "editor" } },
      { path: 'collections', canActivate: [ HasCollectionsGuard ], component: CollectionComponent, data: { routeName: "collections" } },
      { path: '', component: HomeComponent, data: { routeName: "home" } },
      { path: 'download/:uuid', component: DownloadComponent, data: { routeName: "download" } },
      { path: '**', component: HomeComponent },
    ] }
];

function addRouteNames(routes:Array<Route>, base_path = "/")
{
  if (base_path.slice(-1) !== "/") {
    base_path += "/";
  }
  routes.forEach((eachRoute) => {
    let route = {...eachRoute}
    let path = route.path == '**' ? '' : route.path ?? "";
    // path = path.replace(/(:\w+\/?)/g, '')

    if (typeof route.data !== 'undefined') {
      route.path = base_path + path
      RoutenamesModule.routeNamesObject[route.data['routeName'] ?? ""] = route;
    }
    if (typeof route.children !== 'undefined') {
      addRouteNames(route.children, base_path + path);
    }
  })
}

addRouteNames(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
