import { Component } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { RoutenamesModule } from "../../../utils/modules/routenames.module";
import { AuthService } from "../../../auth/services/auth.service";
import { RegisterComponent } from "../../../auth/components/register/register.component";

@Component({
  selector: 'ciren-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class HomeComponent {

  public now = new Date();
  public routeNames = RoutenamesModule;

  constructor(
    public dialog: MatDialog,
    public authService: AuthService
  ) { }

  public openLoginModal()
  {
    this.dialog.open(RegisterComponent)
  }

}
