import { Injectable } from '@angular/core';
import { SaveService } from "../../editor/services/save.service";
import { Router } from "@angular/router";
import { RoutenamesModule } from "../../utils/modules/routenames.module";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private routeNames = RoutenamesModule;

  constructor(
    private saveService: SaveService,
    private router: Router
  ) { }

  public goToEditor()
  {
    let uuid = this.saveService.getLocalStorageSave()
    if (!uuid) {
      this.saveService.createSave();
    } else {
      this.router.navigate([
        this.routeNames.getRoutePath('editor', { uuid })
      ])
    }
  }
}
