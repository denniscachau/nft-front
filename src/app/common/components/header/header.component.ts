import { Component } from '@angular/core';
import { AuthService } from "../../../auth/services/auth.service";
import { RoutenamesModule } from "../../../utils/modules/routenames.module";
import {SaveService} from "../../../editor/services/save.service";
import {Router} from "@angular/router";
import {HomeService} from "../../../home/services/home.service";

@Component({
  selector: 'ciren-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  public routeNames = RoutenamesModule;

  constructor(
    public authService: AuthService,
    public homeService: HomeService
  ) { }

}
