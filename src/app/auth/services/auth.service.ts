import { Injectable } from '@angular/core';
import {Login, Register} from "../interfaces/login";
import { HttpClient } from "@angular/common/http";
import {
  StrapiErrorResponse, StrapiPostResponse,
  StrapiGetResponse,
} from "../../utils/interfaces/strapi-response";
import { HandlersService } from "../../utils/services/handlers.service";
import { User } from "../interfaces/user";
import { map, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: User | null = null;
  private logged: boolean = false;

  constructor(
    private http: HttpClient,
    private handlersService: HandlersService
  ) {
    this.user = localStorage.getItem('user') !== null ? JSON.parse(localStorage.getItem('user') as string) : null;
    this.logged = this.user !== null;
  }

  public login(params: Login): Observable<StrapiPostResponse>
  {
    return this.http.post<StrapiPostResponse>('/auth/local', {
      identifier: params.email,
      password: params.password,
      without_jwt: true
    }).pipe(map((response: StrapiPostResponse) => {
      localStorage.setItem('token', response['jwt']);
      this.user = response['user'] as User;
      localStorage.setItem('user', JSON.stringify(this.user));

      this.logged = true;

      this.handlersService.alert({ type: "success", key: "login.success" });

      return response;
    }))
  }

  public register(params: Register): Observable<StrapiPostResponse>
  {
    return this.http.post<StrapiPostResponse>('/auth/local/register', {
      firstname: params.firstname,
      lastname: params.lastname,
      username: params.email,
      email: params.email,
      password: params.password,
      without_jwt: true
    }).pipe(map((response: StrapiPostResponse) => {
      this.handlersService.alert({ type: "success", key: "register.success" });

      return response;
    }))
  }

  public logout(): void
  {
    this.user = null;
    this.logged = false;
    localStorage.clear();
  }

  public getUser(): User | null
  {
    return this.user;
  }

  public isLogged(): boolean
  {
    return this.logged;
  }

}
