import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from "../../services/auth.service";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {StrapiPostResponse} from "../../../utils/interfaces/strapi-response";
import {User} from "../../interfaces/user";
import {RegisterComponent} from "../register/register.component";

@Component({
  selector: 'ciren-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public loginForm = this.fb.group({
    email: ['', [
      Validators.required, Validators.email
    ]],
    password: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private dialogRef: MatDialogRef<LoginComponent>,
    private dialog: MatDialog
  ) {}

  public onSubmit(): void
  {
    this.authService.login({
      email: this.loginForm.get('email')?.value,
      password: this.loginForm.get('password')?.value
    }).subscribe((response: StrapiPostResponse) => {
      this.dialogRef.close(1);
    })
  }

  public displayRegisterForm(): void
  {
    this.dialogRef.close();
    this.dialog.open(RegisterComponent);
  }
}
