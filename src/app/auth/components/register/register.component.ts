import { Component } from '@angular/core';
import {AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { LoginComponent } from "../login/login.component";
import {StrapiPostResponse} from "../../../utils/interfaces/strapi-response";

@Component({
  selector: 'ciren-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  public registerForm = this.fb.group({
    firstname: ['', [
      Validators.required
    ]],
    lastname: ['', [
      Validators.required
    ]],
    email: ['', [
      Validators.required, Validators.email
    ]],
    password: ['', [
      Validators.required, Validators.minLength(8)
    ]],
    password2: ['', Validators.required]
  });

  public samePasswords = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private dialogRef: MatDialogRef<LoginComponent>,
    private dialog: MatDialog
  ) {}

  public onSubmit(): void
  {
    this.authService.register({
      firstname: this.registerForm.get('firstname')?.value,
      lastname: this.registerForm.get('lastname')?.value,
      email: this.registerForm.get('email')?.value,
      password: this.registerForm.get('password')?.value
    }).subscribe((response: StrapiPostResponse) => {
      // TODO Display a popup saying that the user needs to activate his account
      this.displayLoginForm();
    })
  }

  public displayLoginForm(): void
  {
    this.dialogRef.close();
    this.dialog.open(LoginComponent);
  }

  public inputPasswords()
  {
    this.samePasswords = this.same('password', 'password2');
    this.registerForm.get('password2')!.setErrors(this.samePasswords ? null : {});
  }

  private same(field1: string, field2: string): boolean
  {
    return this.registerForm.get(field1)!.value === this.registerForm.get(field2)!.value;
  }
}
