import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from "@angular/cdk/drag-drop";
import { HomeComponent } from './home/components/home/home.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from "@angular/common/http";
import { MatBottomSheetModule } from "@angular/material/bottom-sheet";
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSliderModule } from "@angular/material/slider";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MdbDropdownModule } from "mdb-angular-ui-kit/dropdown";
import { LayoutModule } from '@angular/cdk/layout';
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { HeaderComponent } from './common/components/header/header.component';
import { FooterComponent } from './common/components/footer/footer.component';
import { EditorComponent } from './editor/components/editor/editor.component';
import { PanelComponent } from './editor/components/panel/panel.component';
import { LayersComponent } from './editor/components/layers/layers.component';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './auth/components/login/login.component';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { HttpInterceptorService } from "./utils/interceptors/http-interceptor.service";
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { LayerPropertiesComponent } from './editor/components/properties/layer-properties/layer-properties.component';
import { ItemPropertiesComponent } from './editor/components/properties/item-properties/item-properties.component';
import { SettingsComponent } from './editor/components/settings/settings.component';
import { RarityPropertiesComponent } from './editor/components/properties/rarity-properties/rarity-properties.component';
import { MatChipsModule } from "@angular/material/chips";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { PreviewComponent } from './editor/components/preview/preview.component';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatRippleModule } from "@angular/material/core";
import { CollectionComponent } from './collection/components/collection/collection.component';
import { MatTableModule } from "@angular/material/table";
import { DownloadComponent } from './download/components/download/download.component';
import { IsAuthGuard } from "./utils/guards/is-auth.guard";
import { HasCollectionsGuard } from "./utils/guards/has-collections.guard";
import { RegisterComponent } from './auth/components/register/register.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    EditorComponent,
    PanelComponent,
    LayersComponent,
    LoginComponent,
    LayerPropertiesComponent,
    ItemPropertiesComponent,
    SettingsComponent,
    RarityPropertiesComponent,
    PreviewComponent,
    CollectionComponent,
    DownloadComponent,
    RegisterComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        DragDropModule,
        HttpClientModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRadioModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSnackBarModule,
        MatTooltipModule,
        MdbDropdownModule,
        LayoutModule,
        TranslateModule.forRoot({
            defaultLanguage: 'en-US',
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ReactiveFormsModule,
        NgxUiLoaderModule,
        FormsModule,
        MatChipsModule,
        MatButtonToggleModule,
        MatProgressSpinnerModule,
        MatRippleModule,
        MatTableModule
    ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
    IsAuthGuard,
    HasCollectionsGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
