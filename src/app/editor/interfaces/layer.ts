import { Item } from "./item";

export interface Layer {
  id?: number;
  items: Item[];
  name: string;
}
