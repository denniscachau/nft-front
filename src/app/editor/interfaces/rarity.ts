export interface Rarity {
  item_id: number;
  rarity: number;
}
