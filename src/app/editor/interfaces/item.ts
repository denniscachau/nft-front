import { StrapiGetResponse } from "../../utils/interfaces/strapi-response";

export interface Item {
  id?: number;
  file: number | StrapiGetResponse;
  name: string;
  position?: number;
  rarity: number;
}
