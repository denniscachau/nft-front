export interface File {
  id?: number;
  name: string;
  mime: string;
  size: number;
  url?: string;
  provider?: string;
}
