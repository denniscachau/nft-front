import { Layer } from "./layer";

export interface Save {
  id: number;
  id_user: number;
  name: string;
  layers: Layer[];
  uuid: string;
}
