import { FormGroup } from "@angular/forms";

export interface Properties {
  propertiesForm: FormGroup
  onClose(): void;
}
