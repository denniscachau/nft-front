export type Media = {
  alternativeText: string;
  caption: string;
  ext: string;
  hash: string;
  height: number;
  mime: string;
  name: string;
  previewUrl: string | null;
  provider: string;
  provider_metadata: string | null;
  size: number;
  url: string;
  width: number;
}
