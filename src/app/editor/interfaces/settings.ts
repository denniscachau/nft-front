export interface Settings {
  resolution: Resolution,
  email: string;
  nb: number;
}

export interface Resolution {
  x: number;
  y: number;
}

export interface MinMax {
  min: number | null;
  max: number | null;
}

export interface ResolutionConfig {
  width: MinMax;
  height: MinMax;
}
