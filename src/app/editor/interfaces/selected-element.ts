import { Item } from "./item";
import { Layer } from "./layer";
import { ComponentType } from "@angular/cdk/overlay";
import { Properties } from "./properties";

export interface SelectedElement {
  component: ComponentType<Properties>;
  properties: Item | Layer
}
