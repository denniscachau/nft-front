import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from "@angular/material/bottom-sheet";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Properties } from "../interfaces/properties";
import { Directive, Inject } from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {SaveService} from "../services/save.service";
import {LayerService} from "../services/layer.service";

@Directive()
export abstract class PropertiesComponent implements Properties {

  abstract propertiesForm: FormGroup;

  constructor(
    protected fb: FormBuilder,
    @Inject(MAT_BOTTOM_SHEET_DATA) public properties: any,
    private _bottomSheetRef: MatBottomSheetRef<PropertiesComponent>,
    protected translate: TranslateService,
    protected saveService: SaveService,
    protected layerService: LayerService
  ) {
    this._bottomSheetRef.backdropClick().subscribe(this.onClose.bind(this))
  }

  protected close()
  {
    this._bottomSheetRef.dismiss();
  }

  public onClose()
  {
    if (this.propertiesForm.valid) {
      this.close();
    }
  }

}
