import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {StrapiPostMultipleResponse} from "../../utils/interfaces/strapi-response";
import {stringify} from "qs";
import {SaveService} from "./save.service";

@Injectable({
  providedIn: 'root'
})
export class PanelService {

  constructor(
    private http: HttpClient,
    private saveService: SaveService
  ) { }

  public deleteFile(fileId: number): void
  {
    const query = stringify({
      uuid: this.saveService.getSave()?.uuid,
      layerId: this.saveService.getSelectedLayer()?.id
    });

    this.http.delete<StrapiPostMultipleResponse>(`/upload-nft/files/${fileId}?${query}`).subscribe();
  }
}
