import { Injectable } from '@angular/core';
import { Settings, Resolution, ResolutionConfig } from "../interfaces/settings";
import { SaveService } from "./save.service";
import { StrapiRequest } from "../../utils/interfaces/strapi-request";
import { Save } from "../interfaces/save";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  public settings: Settings = {
    resolution: {
      x: 0,
      y: 0
    } as Resolution,
    email: "",
    nb: 10
  };

  public resolution_config: ResolutionConfig = {
    width: {
      min: null,
      max: null
    },
    height: {
      min: null,
      max: null
    }
  }

  constructor(
    private saveService: SaveService
  ) { }

  public getResolutionConfig()
  {
    let resolution_config: ResolutionConfig = {
      width: {
        min: null,
        max: null
      },
      height: {
        min: null,
        max: null
      }
    }

    let save = this.saveService.getSave() as Save;
    let layers = save.layers;

    for (let layer of layers) {
      for (let item of layer.items) {
        let w = (item.file as StrapiRequest).data['attributes'].width as number;
        let h = (item.file as StrapiRequest).data['attributes'].height as number;

        resolution_config = {
          width: {
            min: resolution_config.width.min == null || resolution_config.width.min > w ? w : resolution_config.width.min,
            max: resolution_config.width.max == null || resolution_config.width.max < w ? w : resolution_config.width.max
          },
          height: {
            min: resolution_config.height.min == null || resolution_config.height.min > h ? h : resolution_config.height.min,
            max: resolution_config.height.max == null || resolution_config.height.max < h ? h : resolution_config.height.max
          }
        }

      }
    }

    return resolution_config;
  }

  public setResolutionConfig()
  {
    this.resolution_config = this.getResolutionConfig();
  }

}
