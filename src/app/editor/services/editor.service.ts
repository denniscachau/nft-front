import {Component, Injectable} from '@angular/core';
import { SelectedElement } from "../interfaces/selected-element";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import { Save } from "../interfaces/save";
import {ItemPropertiesComponent} from "../components/properties/item-properties/item-properties.component";
import {LayerPropertiesComponent} from "../components/properties/layer-properties/layer-properties.component";
import {PropertiesComponent} from "../abstracts/properties.component";
import {Properties} from "../interfaces/properties";
import {ComponentType} from "@angular/cdk/overlay";
import {Observable, Subject} from "rxjs";
import {StrapiPostMultipleResponse} from "../../utils/interfaces/strapi-response";
import {File as CirenFile} from "../interfaces/file";
import {SaveService} from "./save.service";

@Injectable({
  providedIn: 'root'
})
export class EditorService {

  public isLoading: boolean = false;

  private selected_element: SelectedElement|null = null;

  constructor(
    private _bottomSheet: MatBottomSheet,
    private saveService: SaveService
  ) {
    this.saveService.OpenPropertiesSheet.subscribe(this.setSelectedElement.bind(this));
  }

  public getSelectedElement()
  {
    return this.selected_element;
  }

  public setSelectedElement(selectedElement: SelectedElement): Observable<SelectedElement> | null
  {
    if (this.selected_element != selectedElement) {
      this.selected_element = selectedElement;
      return this.openPropertiesSheet();
    }
    return null;
  }

  private openPropertiesSheet(): Observable<SelectedElement> | null
  {
    if (this.selected_element) {
      return this._bottomSheet.open(this.selected_element.component, {
        disableClose: true,
        data: this.selected_element.properties
      }).afterDismissed()
    }

    return null;
  }

  public uploadFiles(files: File[])
  {
    const nb_files = files.length;
    let i = 0;
    this.isLoading = true;
    for (let file of files) {
      this.saveService.uploadFile(file).subscribe({
        next: (res: StrapiPostMultipleResponse) => {
          const f = res[0] as CirenFile;
          this.saveService.addItem({
            name: file.name,
            rarity: 0,
            file: {
              data: {
                attributes: f,
                id: f.id as number
              }
            }
          })
          i++;
          if (i >= nb_files) {
            this.saveService.Save.next();
            this.isLoading = false;
          }
        }
      });
    }
  }
}
