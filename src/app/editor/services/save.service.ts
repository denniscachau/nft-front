import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { stringify } from 'qs'
import {
  Data,
  StrapiGetResponse,
  StrapiPostMultipleResponse, StrapiPostResponse
} from "../../utils/interfaces/strapi-response";
import { Save } from "../interfaces/save";
import { Layer } from "../interfaces/layer";
import { Observable, Subject, takeUntil } from "rxjs";
import { Item } from "../interfaces/item";
import { moveItemInArray } from "@angular/cdk/drag-drop";
import { SelectedElement } from "../interfaces/selected-element";
import { LayerPropertiesComponent } from "../components/properties/layer-properties/layer-properties.component";
import { Settings } from "../interfaces/settings";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { RoutenamesModule } from "../../utils/modules/routenames.module";

@Injectable({
  providedIn: 'root'
})
export class SaveService {

  private save?: Save;
  private selected_layer?: Layer;
  private routeNames = RoutenamesModule;

  public OnLayerChange: Subject<void> = new Subject();
  public Save: Subject<void> = new Subject();
  public OpenPropertiesSheet: Subject<SelectedElement> = new Subject();
  public DeleteItem: Subject<Item> = new Subject();
  public DeleteLayer: Subject<Layer> = new Subject();

  private stopSaving: Subject<void> = new Subject();

  constructor(
    private http: HttpClient,
    private translateService: TranslateService,
    private router: Router
  ) {
    this.Save.subscribe(() => {
      this.stopSaving.next();
      this.sendSave();
    });
  }

  public initSave(uuid: string)
  {
    const query = stringify({
      uuid,
      populate: [
        'layers',
        'layers.items',
        'layers.items.file'
      ]
    });

    return this.http.get<StrapiGetResponse>(`/saves/uuid?${query}`);
  }

  public getSave()
  {
    return this.save;
  }

  public setSave(id: number, save: Save)
  {
    save.id = id;
    this.save = save;

    this.setSelectedLayer(this.save.layers[0])
  }

  private sendSave()
  {
    let save = JSON.parse(JSON.stringify(this.save)) as Save;
    for (let layer of save.layers) {
      for (let item of layer.items) {
        if (typeof item.file == "object" && "data" in item.file) {
          item.file = item.file.data['id'] ?? 0;
        }
      }
    }

    const query = stringify({
      populate: [
        'layers',
        'layers.items',
        'layers.items.file'
      ]
    });

    this.http.put<StrapiGetResponse>(`/saves/${save.id}?${query}`, { data: save })
      .pipe(takeUntil(this.stopSaving))
      .subscribe((response: StrapiGetResponse) => {
        (this.save as Save).layers.forEach( ( layer, index ) => {
          if (typeof layer.id === "undefined" && typeof response.data.attributes['layers'][index].id !== "undefined") {
            layer.id = response.data.attributes['layers'][index].id
          }
        });

        // this.save = response.data.attributes as Save;
        (this.save as Save).id = response.data.id;
        // this.setSelectedLayer(this.selected_layer as Layer);
      })
  }

  public getLayers(): Layer[]
  {
    return this.save?.layers ?? [];
  }

  public setLayers(layers: Layer[])
  {
    this.save!.layers = layers;
  }

  public setSelectedLayer(layer: Layer)
  {
    this.selected_layer = this.save?.layers.find((l) => {
      return l == layer;
    }) as Layer;
    this.OnLayerChange.next();
  }

  public getSelectedLayer()
  {
    return this.selected_layer;
  }

  public getItems()
  {
    return this.selected_layer?.items ?? [];
  }

  public setItems(items: Item[])
  {
    if (this.selected_layer) {
      this.selected_layer.items = items;
    }
  }

  public addLayer()
  {
    const empty_layer = {
      name: "",
      items: []
    };
    let cell = this.save?.layers.push(empty_layer) as number;

    this.setSelectedLayer(this.save?.layers[cell-1] as Layer);
    this.OpenPropertiesSheet.next({
        component: LayerPropertiesComponent,
        properties: this.selected_layer as Layer
      }
    );
  }

  public addItems(items: Item[])
  {
    for (let item of items) {
      this.addItem(item);
    }
  }

  public addItem(item: Item)
  {
    this.selected_layer?.items.push(item);
  }

  public uploadFile(file: any): Observable<StrapiPostMultipleResponse>
  {
    let form_data = new FormData();
    form_data.set('files', file);

    const query = stringify({
      uuid: this.save?.uuid,
      layerId: this.selected_layer?.id
    });

    return this.http.post<StrapiPostMultipleResponse>(`/upload?${query}`, form_data);
  }

  public changePosition(previousPosition: number, currentPosition: number)
  {
    if (previousPosition !== currentPosition) {
      moveItemInArray(this.save?.layers as Layer[], previousPosition, currentPosition);
      this.Save.next();
    }
  }

  public generate(settings: Settings): Observable<StrapiGetResponse>
  {
    const query = stringify({
      uuid: this.save!.uuid
    });

    return this.http.post<StrapiGetResponse>(`/nft/generate?${query}`, settings)
  }

  public preview(body: Settings): Observable<StrapiGetResponse>
  {
    const query = stringify({
      uuid: this.save!.uuid
    });

    return this.http.post<StrapiGetResponse>(`/nft/preview?${query}`, body)
  }

  public getLocalStorageSave()
  {
    return localStorage.getItem('save-uuid');
  }

  public createSave(name: string | null = null): void
  {
    this.http.post<StrapiGetResponse>(`/saves`, {
      data: {
        layers: [
          { name: this.translateService.instant('editor.layers.background'), items: [] }
        ],
        name: name
      }
    })
      .subscribe((response) => {
      let uuid = response.data.attributes['uuid'];
      localStorage.setItem('save-uuid', uuid)
      this.router.navigate([
        this.routeNames.getRoutePath('editor', { uuid })
      ])
    });
  }

  public deleteSave(saveId: number | null = null): Observable<StrapiPostResponse>
  {
    if (saveId === null) {
      saveId = this.save!.id;
    }

    return this.http.delete(`/saves/${saveId}`)
  }

}

