import { Injectable } from '@angular/core';
import {SaveService} from "./save.service";
import {Layer} from "../interfaces/layer";
import {Item} from "../interfaces/item";
import {StrapiGetResponse} from "../../utils/interfaces/strapi-response";

@Injectable({
  providedIn: 'root'
})
export class LayerService {

  constructor(
    private saveService: SaveService
  ) {
    this.saveService.DeleteLayer.subscribe(this.deleteLayer.bind(this))
  }

  private deleteLayer(layer: Layer)
  {
    const layers = this.saveService.getLayers().filter((l) => l !== layer) as Layer[];
    console.log("on delete layer", layers);
    this.saveService.setSelectedLayer(layers[0]);
    this.saveService.setLayers(layers);
    this.saveService.Save.next();
  }

}
