import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { SettingsService } from "../../services/settings.service";
import { SaveService } from "../../services/save.service";
import { MatDialogRef } from "@angular/material/dialog";
import { AlertService } from "../../../utils/services/alert.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'ciren-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {

  public settingsForm = this.fb.group({
    resolutionX: [ this.settingsService.settings.resolution.x, [
      Validators.required, Validators.min(1)
    ] ],
    resolutionY: [ this.settingsService.settings.resolution.y, [
      Validators.required, Validators.min(1)
    ] ],
    email: [this.settingsService.settings.email, [
      Validators.required, Validators.email
    ]],
    nb: [this.settingsService.settings.nb, [
      Validators.required, Validators.max(10000), Validators.min(1)
    ]]
  });

  constructor(
    public settingsService: SettingsService,
    private fb: FormBuilder,
    private saveService: SaveService,
    private dialogRef: MatDialogRef<SettingsComponent>,
    private alertService: AlertService,
    private translate: TranslateService
  ) {
    this.settingsService.setResolutionConfig();
  }

  public onSubmit()
  {
    this.saveService.generate(this.settingsService.settings)
      .subscribe((res) => {
        this.dialogRef.close();
        this.alertService.alert({
          type: "success",
          message: this.translate.instant('download.send-email')
        })
      })
  }

}
