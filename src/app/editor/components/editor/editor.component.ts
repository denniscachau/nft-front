import { Component, ElementRef, OnInit, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';
import { EditorService } from "../../services/editor.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { RoutenamesModule } from "../../../utils/modules/routenames.module";
import { HandlersService } from "../../../utils/services/handlers.service";
import { SaveService } from "../../services/save.service";
import { StrapiGetResponse } from "../../../utils/interfaces/strapi-response";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { Save } from "../../interfaces/save";
import { MatDialog } from "@angular/material/dialog";
import { SettingsComponent } from "../settings/settings.component";
import { PreviewComponent } from "../preview/preview.component";
import { SettingsService } from "../../services/settings.service";

@Component({
  selector: 'ciren-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditorComponent implements OnInit  {

  @ViewChild('dropOverlay') dropOverlay!: ElementRef

  constructor(
    public saveService: SaveService,
    private editorService: EditorService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private handlersService: HandlersService,
    private ngxLoader: NgxUiLoaderService,
    private renderer: Renderer2,
    private dialog: MatDialog,
    private settingsService: SettingsService
  ) { }

  public ngOnInit()
  {
    this.ngxLoader.start();
    this.activatedRoute.params.subscribe((params: Params) => {
      let uuid = params['uuid'];
      if (uuid) {
        this.saveService.initSave(uuid).subscribe({
          next: (response: StrapiGetResponse) => this.saveService.setSave(response.data.id, response.data.attributes as Save),
          error: () => this.router.navigate([RoutenamesModule.getRoutePath('home')]).then(() => {
            this.ngxLoader.stop();
            this.handlersService.alert({ type: "danger", key: "alert.error.forbidden-resource" });
          }),
          complete: () => this.ngxLoader.stop()
        });
      } else {
        this.router.navigate([RoutenamesModule.getRoutePath('home')]).then(() => {
          this.ngxLoader.stop();
          this.handlersService.alert({ type: "danger", key: "alert.error.not-found" });
        })
      }
    })
  }

  public onDragOver(event: any)
  {
    event.preventDefault();
    // console.log(event.target.classList, event.target)
    if (event.type == "dragover") {
      event.stopPropagation();
      this.renderer.removeClass(this.dropOverlay.nativeElement, 'd-none');
      this.renderer.addClass(this.dropOverlay.nativeElement, 'd-flex');
    } else {
      console.log(event.target.classList.value);
      event.stopPropagation();
      this.renderer.addClass(this.dropOverlay.nativeElement, 'd-none');
      this.renderer.removeClass(this.dropOverlay.nativeElement, 'd-flex');
    }
  }

  public onDrop(event: any)
  {
    this.onDragOver(event);

    let files = event.target.files || event.dataTransfer.files;
    this.editorService.uploadFiles(files);
  }

  public generate()
  {
    this.settingsService.setResolutionConfig();
    this.settingsService.settings.resolution = {
      x: this.settingsService.resolution_config.width.max as number,
      y: this.settingsService.resolution_config.height.max as number
    }
    this.dialog.open(SettingsComponent, {
      width: '500px',
      maxWidth: '100vw'
    });
  }

  public preview()
  {
    this.settingsService.setResolutionConfig();
    this.settingsService.settings.resolution = {
      x: this.settingsService.resolution_config.width.max as number,
      y: this.settingsService.resolution_config.height.max as number
    }
    this.dialog.open(PreviewComponent, {
      width: `${this.settingsService.settings.resolution.x+48}px`,
      height: `${this.settingsService.settings.resolution.y+48}px`,
      maxWidth: '80vw',
      maxHeight: '80vh',
    });
  }

}
