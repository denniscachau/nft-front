import {Component, Inject} from '@angular/core';
import { PropertiesComponent } from "../../../abstracts/properties.component";
import {FormBuilder, Validators} from "@angular/forms";
import {Item} from "../../../interfaces/item";
import {StrapiGetResponse} from "../../../../utils/interfaces/strapi-response";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {TranslateService} from "@ngx-translate/core";
import {Layer} from "../../../interfaces/layer";

@Component({
  selector: 'ciren-layer-properties',
  templateUrl: './layer-properties.component.html',
  styleUrls: ['./layer-properties.component.scss']
})
export class LayerPropertiesComponent extends PropertiesComponent {

  public propertiesForm = this.fb.group({
    name: [ this.properties.name, Validators.required ],
    // layer: [ null, Validators.required ]
  });

  public onDelete(layer: Layer)
  {
    this.translate.get('alert.delete-confirm').subscribe((trans) => {
      if (confirm(trans)) {
        this.close();
        this.saveService.DeleteLayer.next(layer);
      }
    })
  }

}
