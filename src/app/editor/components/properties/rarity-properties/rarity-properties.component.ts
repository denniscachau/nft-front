import { Component, Inject, OnInit } from '@angular/core';
import { PropertiesComponent } from "../../../abstracts/properties.component";
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from "@angular/material/bottom-sheet";
import { Item } from "../../../interfaces/item";
import { MatSliderChange } from "@angular/material/slider";
import { Rarity } from "../../../interfaces/rarity";
import { SaveService } from "../../../services/save.service";

@Component({
  selector: 'ciren-rarity-properties',
  templateUrl: './rarity-properties.component.html',
  styleUrls: ['./rarity-properties.component.scss']
})
export class RarityPropertiesComponent {

  public rarities: Rarity[] = [];
  public behaviour: "relative" | "absolute" = "relative";
  public nb_items: number;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public items: Item[],
    private _bottomSheetRef: MatBottomSheetRef<PropertiesComponent>,
    private saveService: SaveService
  ) {
    this.nb_items = this.items.length;
  }

  // public ngOnInit()
  // {
  //   const nbItemsWithRarity = this.items
  //     .filter((i) => i.rarity > 0);
  //   const totalRarity = nbItemsWithRarity.length === 0 ? 0 : nbItemsWithRarity
  //     .map((i) => i.rarity)
  //     .reduce((a, b) => a+b);
  //
  //   const defaultRarity = 100-totalRarity > 0 ? (100-totalRarity) / this.items.filter((i) => i.rarity === 0).length : 1;
  //   this.rarities = [];
  //   for (let item of this.items) {
  //     item.rarity = item.rarity > 0 ? Math.round(item.rarity*100)/100 : Math.round(defaultRarity*100)/100;
  //
  //     this.rarities.push({
  //       item_id: item.id as number,
  //       rarity: item.rarity
  //     });
  //   }
  // }

  public close()
  {
    this._bottomSheetRef.dismiss();
  }

  // TODO add absolute behaviour
  public getRarity(item: Item)
  {
    switch (this.behaviour) {
      case "relative":
        return item.rarity;
      case "absolute":
        return this.rarities.find((i) => i.item_id === item.id)?.rarity ?? item.rarity;
    }
  }

  public onRarityInput(event: MatSliderChange, item: Item)
  {
    item.rarity = event.value as number;
    const max_rarity = 100-item.rarity;

    const items = this.items
      .filter((i) => i !== item);

    if (items.length === 0) {
      return;
    }

    const total_rarity = items
      .map((i) => i.rarity)
      .reduce((a, b) => a+b);

    items
      .forEach((i) => {
        i.rarity = Math.round(i.rarity / total_rarity * max_rarity * 10) / 10;
      });
  }

  public onRarityChange()
  {
    this.saveService.Save.next();
  }
}
