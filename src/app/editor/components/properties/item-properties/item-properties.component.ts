import { Component } from '@angular/core';
import { PropertiesComponent } from "../../../abstracts/properties.component";
import {Validators} from "@angular/forms";

@Component({
  selector: 'ciren-item-properties',
  templateUrl: './item-properties.component.html',
  styleUrls: ['./item-properties.component.scss']
})
export class ItemPropertiesComponent extends PropertiesComponent {

  public propertiesForm = this.fb.group({
    name: [ this.properties.name, Validators.required ],
    // layer: [ null, Validators.required ]
  });

}
