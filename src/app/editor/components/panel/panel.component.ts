import { Component, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { Item } from "../../interfaces/item";
import { environment } from "../../../../environments/environment";
import { ItemPropertiesComponent } from "../properties/item-properties/item-properties.component";
import { SaveService } from "../../services/save.service";
import { EditorService } from "../../services/editor.service";
import { File as CirenFile } from "../../interfaces/file";
import { PanelService } from "../../services/panel.service";
import { StrapiGetResponse } from "../../../utils/interfaces/strapi-response";
import { TranslateService } from "@ngx-translate/core";
import { MatCheckboxChange } from "@angular/material/checkbox";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import { RarityPropertiesComponent } from "../properties/rarity-properties/rarity-properties.component";

@Component({
  selector: 'ciren-editor-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PanelComponent {

  @Output() onSideBarToggle = new EventEmitter<void>();

  public upload_base_url = environment.UPLOAD_BASE_URL;
  public items: Item[] = [];
  public selected_item: Item | null = null;
  public itemsChecked: Item[] = [];

  constructor(
    public saveService: SaveService,
    public editorService: EditorService,
    private panelService: PanelService,
    private translate: TranslateService,
    private _bottomSheet: MatBottomSheet
  ) {
    this.saveService.OnLayerChange.subscribe(this.onLayerChange.bind(this));
    this.saveService.DeleteItem.subscribe(this.deleteItem.bind(this));
  }

  public onItemEdit($event: Event, element: Item)
  {
    if (this.selected_item != element) {
      this.selected_item = element;
      this.editorService.setSelectedElement({
        component: ItemPropertiesComponent,
        properties: element
      })?.subscribe(() => {
        this.selected_item = this.selected_item != element ? this.selected_item : null;
        this.saveService.Save.next();
      })
    }

    $event.stopPropagation();
  }

  public onItemDelete($event: Event, element: Item)
  {
    this.translate.get('alert.delete-confirm').subscribe((trans) => {
      if (confirm(trans)) {
        this.saveService.DeleteItem.next(element);
      }
    });

    $event.stopPropagation();
  }

  private onLayerChange()
  {
    this.items = this.saveService.getItems();
    this.itemsChecked = [];
  }

  public onUploadFiles($event: any)
  {
    let files = ($event.target?.files ?? []) as File[];

    this.editorService.uploadFiles(files);
  }

  public getFile(item: Item): CirenFile
  {
    if (typeof item.file == "object" && typeof item.file.data !== "undefined" && item.file.data !== null) {
      return item.file.data.attributes as CirenFile;
    } else {
      return {
        name: "",
        mime: "",
        size: 0
      }
    }
  }

  private deleteItem(item: Item)
  {
    this.items = this.items.filter((i) => i !== item) as Item[];
    this.itemsChecked = this.itemsChecked.filter((i) => i !== item) as Item[];
    this.saveService.setItems(this.items);
    this.saveService.Save.next();
    this.panelService.deleteFile((item.file as StrapiGetResponse).data.id);
  }

  public onCheckboxChange(event: MatCheckboxChange, item: Item)
  {
    if (event.checked) {
      this.itemsChecked.push(item);
    } else {
      this.itemsChecked = this.itemsChecked.filter((i) => i != item);
    }
  }

  public onBulkRarity()
  {
    this._bottomSheet.open(RarityPropertiesComponent, {
      disableClose: false,
      data: this.items
    })
  }

  public onBulkDelete()
  {
    if (window.confirm(this.translate.instant('alert.bulk-delete-confirm'))) {
      for (let item of this.itemsChecked) {
        this.saveService.DeleteItem.next(item);
      }
    }
  }

  public onSelectAll()
  {
    if (this.itemsChecked.length === this.items.length) {
      this.itemsChecked = [];
    } else {
      this.itemsChecked = this.items;
    }
  }

}
