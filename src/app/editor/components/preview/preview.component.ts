import { Component, OnInit } from '@angular/core';
import { SaveService } from "../../services/save.service";
import { environment } from "../../../../environments/environment";
import {SettingsService} from "../../services/settings.service";
import {NgxUiLoaderService} from "ngx-ui-loader";

@Component({
  selector: 'ciren-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  public image: string | null = null;
  public upload_base_url = environment.UPLOAD_BASE_URL;

  constructor(
    private saveService: SaveService,
    private settingsService: SettingsService,
    private ngxLoader: NgxUiLoaderService
  ) { }

  ngOnInit(): void {
    this.ngxLoader.startLoader('preview_modal');
    this.saveService.preview(this.settingsService.settings).subscribe((p) => {
      this.image = this.upload_base_url + p.data['src'];
      this.ngxLoader.stopLoader('preview_modal');
    });
  }

}
