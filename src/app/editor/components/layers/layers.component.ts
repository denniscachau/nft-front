import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {SelectedElement} from "../../interfaces/selected-element";
import {Layer} from "../../interfaces/layer";
import {SaveService} from "../../services/save.service";
import {LayerPropertiesComponent} from "../properties/layer-properties/layer-properties.component";
import {EditorService} from "../../services/editor.service";
import {CdkDragDrop, moveItemInArray,} from "@angular/cdk/drag-drop";

@Component({
  selector: 'ciren-editor-layers',
  templateUrl: './layers.component.html',
  styleUrls: ['./layers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayersComponent {

  @Input() layers: Layer[] = [];

  constructor(
    public saveService: SaveService,
    private editorService: EditorService
  ) { }

  public onLayerClick(layer: Layer)
  {
    this.saveService.setSelectedLayer(layer);
  }

  public onLayerEdit($event: Event, element: Layer)
  {
    this.editorService.setSelectedElement({
      component: LayerPropertiesComponent,
      properties: element
    })?.subscribe(() => this.saveService.Save.next())

    // Edit a layer will display all his items
    // $event.stopPropagation();
  }

  public addNewLayer()
  {
    this.saveService.addLayer();
  }

  drop(event: CdkDragDrop<string[]>) {
    this.saveService.changePosition(event.previousIndex, event.currentIndex);
  }

}
