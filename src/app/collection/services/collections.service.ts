import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { StrapiGetMultipleResponse } from "../../utils/interfaces/strapi-response";

@Injectable({
  providedIn: 'root'
})
export class CollectionsService {

  constructor(
    private http: HttpClient
  ) { }

  public getCollections(): Observable<StrapiGetMultipleResponse>
  {
    return this.http.get<StrapiGetMultipleResponse>('/saves');
  }

}
