import { Component } from '@angular/core';
import { Save } from "../../../editor/interfaces/save";
import { CollectionsService } from "../../services/collections.service";
import { RoutenamesModule } from "../../../utils/modules/routenames.module";
import {SaveService} from "../../../editor/services/save.service";
import {AlertService} from "../../../utils/services/alert.service";
import {HandlersService} from "../../../utils/services/handlers.service";

@Component({
  selector: 'ciren-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent {

  public routeNames = RoutenamesModule;

  public collections: Save[] = [];
  public columns: string[] = [
    "name",
    // "createdAt",
    "generated",
    // "generated_at",
    "updatedAt",
    "actions"
  ];

  constructor(
    private collectionsService: CollectionsService,
    private saveService: SaveService,
    private handlersService: HandlersService
  ) {
    this.collectionsService.getCollections().subscribe((response) => {
      this.collections = [];
      for (let save of response.data) {
        this.collections.push({
          id: save.id,
          ...save.attributes
        } as Save)
      }
    })
  }

  public deleteSave(saveId: number)
  {
    if (this.handlersService.confirm("alert.delete-confirm")) {
      this.saveService
        .deleteSave(saveId)
        .subscribe(() => {
          this.collections = this.collections.filter((c) => c.id !== saveId);
          this.handlersService.alert({
            type: "success",
            key: "alert.success.delete"
          })
        })
    }
  }

  public createSave()
  {
    this.saveService.createSave();
  }

}
