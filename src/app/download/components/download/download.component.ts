import { Component, OnInit } from '@angular/core';
import {DownloadService} from "../../services/download.service";
import {ActivatedRoute} from "@angular/router";
import {NgxUiLoaderService} from "ngx-ui-loader";

@Component({
  selector: 'ciren-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

  public link: string = "";

  constructor(
    private downloadService: DownloadService,
    private activatedRoute: ActivatedRoute,
    private ngxLoader: NgxUiLoaderService
  ) { }

  public ngOnInit()
  {
    this.ngxLoader.start();
    this.activatedRoute.params.subscribe((params) => {
      const uuid: string = params['uuid'];

      this.link = this.downloadService.getDownloadLink(uuid);
      this.ngxLoader.stop();
    })
  }

}
