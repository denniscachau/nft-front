import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  private upload_base_url = environment.UPLOAD_BASE_URL;

  constructor() { }

  public getDownloadLink(uuid: string)
  {
    return `${this.upload_base_url}/build/${uuid}/images.zip`
  }

}
